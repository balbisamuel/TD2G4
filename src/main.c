/* Copyright 2015, Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of lpc1769_template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Blinky using FreeRTOS.
 *
 *
 * NOTE: It's interesting to check behavior differences between standard and
 * tickless mode. Set @ref configUSE_TICKLESS_IDLE to 1, increment a counter
 * in @ref vApplicationTickHook and print the counter value every second
 * inside a task. In standard mode the counter will have a value around 1000.
 * In tickless mode, it will be around 25.
 *
 */

/** \addtogroup rtos_blink FreeRTOS blink example
 ** @{ */

/*==================[inclusions]=============================================*/

#include "main.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

static uint32_t cursor;
static uint32_t fila;
static uint16_t valor_adc;


xSemaphoreHandle sem_adc;


typedef enum Estados {OFF=0, ON, HOLD} ESTADOS;

typedef enum Velocidad {SLOW=0, FAST } VELOCIDAD;

typedef enum Tipo {NORMAL=0, MAX, MIN } TIPO;

ESTADOS estado_menu;
VELOCIDAD velocidad_muestreo;
TIPO operacion;

/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);

//static void configteclado();

static void configlcd();

/** @brief delay function
 * @param port PUERTO
 * @param pin  PIN
 * @param state true = 1 / false = 0
 */
void Set_pin(uint32_t port, uint32_t pin, bool state);

void Print_LCD(char *data);

/** @brief delay function
 * @param data dato a escribir
 * @param type true = dato / false = instruccion
 */
static void Write_LCD(uint32_t data,bool type);

static void Clear_LCD();


static void adcInit(void);

static uint32_t Convertir_DB(uint32_t promedio);


/** @brief delay function
 * @param data dato a escribir
 */
static void Write_LCD_Data(uint32_t data);

static void comprobarcursor();

/** @brief delay function
 * @param data instruccion a escribir
  */
static void Write_LCD_Command(uint32_t data);

//static void adcInit(void);

//static void leerteclado();



/*==================[internal data definition]===============================*/


/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void initHardware(void)
{

	Board_Init();
	SystemCoreClockUpdate();

	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 2, 9);
	Chip_GPIO_SetPinOutLow(LPC_GPIO, 2, 9);

}


static void configlcd()
{

		Chip_GPIO_WriteDirBit(LPC_GPIO, LCD_D4 , true);
		Chip_GPIO_WriteDirBit(LPC_GPIO, LCD_D5 , true);
		Chip_GPIO_WriteDirBit(LPC_GPIO, LCD_D6 , true);
		Chip_GPIO_WriteDirBit(LPC_GPIO, LCD_D7 , true);
		Chip_GPIO_WriteDirBit(LPC_GPIO, LCD_EN , true);
		Chip_GPIO_WriteDirBit(LPC_GPIO, LCD_RS , true);

		vTaskDelay(DELAY_LCD_INIT_MS / portTICK_RATE_MS);
		Write_LCD((LCD_4BITS>>4),LCD_INS);
		vTaskDelay(DELAY_LCD_MS / portTICK_RATE_MS);
		Write_LCD_Command(LCD_2LINEAS);
		Write_LCD_Command(LCD_ON);
		Write_LCD_Command(CURSOR_START);
		Write_LCD_Command(CLR_SCREEN);

		cursor = 0;
		fila = FILA1;

}

void Write_LCD(uint32_t data, bool type)
{

		Set_pin(LCD_RS, type);
		Set_pin(LCD_EN,true);

		Set_pin(LCD_D4, data &1);
		Set_pin(LCD_D5,(data >>1)&1);
		Set_pin(LCD_D6,(data >>2)&1);
		Set_pin(LCD_D7,(data >>3)&1);

		Set_pin(LCD_EN,false);

}

static void Clear_LCD()
{
	Write_LCD_Command(CLR_SCREEN);
	cursor=0;
	fila=FILA1;
}

static void Return_Home_LCD()
{

	Write_LCD_Command(CURSOR_START);
	cursor = 0;
	fila = FILA1;

}

void Print_LCD(char *data)
{
	uint8_t i;



	for (i=0;i<strlen(data);i++)
	{
		cursor++;
		comprobarcursor();
		Write_LCD_Data(data[i]);
	}
}

static void comprobarcursor()
{
	if (cursor > COLUMNAS_MAX)
	{
		cursor=0;
		if (fila)
		{
			Write_LCD_Command(LINEA1);
			fila = FILA1;
		}
		else
		{
			Write_LCD_Command(LINEA2);
			fila = FILA2;
     	}

	}




}

static void Write_LCD_Command(uint32_t data)
{

		Write_LCD((data>>4),LCD_INS);
		vTaskDelay(DELAY_LCD_MS / portTICK_RATE_MS);
		Write_LCD(data,LCD_INS);
		vTaskDelay(DELAY_LCD_MS / portTICK_RATE_MS);

}

static void Write_LCD_Data(uint32_t data)
{

		Write_LCD((data>>4),LCD_DATA);
		vTaskDelay(DELAY_LCD_MS / portTICK_RATE_MS);
		Write_LCD(data,LCD_DATA);
		vTaskDelay(DELAY_LCD_MS / portTICK_RATE_MS);

}


void Set_pin(uint32_t port, uint32_t pin, bool state)
{
	Chip_GPIO_WritePortBit(LPC_GPIO, port, pin, state);
}

static void configteclado()
{
	Chip_GPIO_WriteDirBit(LPC_GPIO, BOTON_ON_OFF , false);
	Chip_GPIO_WriteDirBit(LPC_GPIO, BOTON_FAST_SLOW, false);
	Chip_GPIO_WriteDirBit(LPC_GPIO, BOTON_MAX_MIN , false);
	Chip_GPIO_WriteDirBit(LPC_GPIO, BOTON_HOLD , false);
}

//AD0.0 P0.23

static void adcInit(void)
{
	ADC_CLOCK_SETUP_T adc;

	Chip_ADC_Init(LPC_ADC, &adc);

	Chip_IOCON_PinMux(LPC_IOCON, 0, 23, IOCON_MODE_PULLUP, IOCON_FUNC1);

	Chip_ADC_SetSampleRate(LPC_ADC, &adc, 88000);

	Chip_ADC_EnableChannel(LPC_ADC, ADC_CH0, ENABLE);
	Chip_ADC_Int_SetChannelCmd(LPC_ADC, ADC_CH0, ENABLE);
	Chip_ADC_SetBurstCmd(LPC_ADC, ENABLE);

	NVIC_EnableIRQ(ADC_IRQn);
}


void ADC_IRQHandler(void)
{
	portBASE_TYPE contexto = pdFALSE;

	static uint16_t data;

	Chip_ADC_ReadValue(LPC_ADC, ADC_CH0, &data);

	valor_adc = data;

	xSemaphoreGiveFromISR(sem_adc, &contexto);

	portEND_SWITCHING_ISR(contexto);

}

static uint32_t Convertir_DB(uint32_t promedio)
{


	if (promedio >= 40 && promedio < 88)
	{
		return (-7.85714 + 0.71429*promedio);
	}

	if (promedio >= 88 && promedio < 98)
	{
		return (11 + 0.5*promedio);
	}

	if (promedio >= 98 && promedio < 125)
	{
		return (41.85185 + 0.18519*promedio);
	}

	if (promedio >= 125 && promedio < 160)
	{
		return (47.14286 + 0.14286*promedio);
	}

	if (promedio >= 160 && promedio < 322)
	{
		return (65.06173 + 0.03086*promedio);
	}

	if (promedio >= 322 && promedio >= 1000 )
	{
		return (69.77273 + 0.01623*promedio);
	}



return 0;


/*

	MEDIDAS ENTRE 35-80 DB

	CUENTAS ENTRE 60-88 (35-55 DB)
	-7.85714+0.71429x

	CUENTAS ENTRE 88-98 (55-60 DB)
	11+0.5x

	CUENTAS ENTRE 98-125 (60-65 DB)
	41.85185+0.18519x

	CUENTAS ENTRE 125-160 (65-70 DB)
	47.14286+0.14286x

	CUENTAS ENTRE 160-322 (70-75 DB)
	65.06173+0.03086X

	CUENTAS ENTRE 322-630 (75-80 DB)
	69.77273+0.01623x

*/


}



static void leerteclado()
{

	if(Chip_GPIO_GetPinState(LPC_GPIO, BOTON_ON_OFF)==BOTON_ON)
	{
		vTaskDelay(DELAY_REBOTE_MS / portTICK_RATE_MS);
		if(Chip_GPIO_GetPinState(LPC_GPIO, BOTON_ON_OFF)==BOTON_ON)
		{
			if (estado_menu == ON)
			{
				estado_menu = OFF;
				Clear_LCD();
				Print_LCD("Apagando...");
				vTaskDelay(500/ portTICK_RATE_MS);
				Print_LCD(".");
				vTaskDelay(200/ portTICK_RATE_MS);
				Print_LCD(".");
				vTaskDelay(200/ portTICK_RATE_MS);
				Print_LCD(".");
				vTaskDelay(500/ portTICK_RATE_MS);
			}
			else if (estado_menu == OFF)
			{
				estado_menu = ON;
				Print_LCD("Bienvenido");
				vTaskDelay(500/ portTICK_RATE_MS);
				Print_LCD(".");
				vTaskDelay(200/ portTICK_RATE_MS);
				Print_LCD(".");
				vTaskDelay(200/ portTICK_RATE_MS);
				Print_LCD(".");
				vTaskDelay(500/ portTICK_RATE_MS);
				Clear_LCD();

			}
		}
	}

	if(Chip_GPIO_GetPinState(LPC_GPIO, BOTON_FAST_SLOW)==BOTON_ON)
	{
		vTaskDelay(DELAY_REBOTE_MS / portTICK_RATE_MS);
		if(Chip_GPIO_GetPinState(LPC_GPIO, BOTON_FAST_SLOW)==BOTON_ON)
		{
			if (velocidad_muestreo == SLOW)
			{

				velocidad_muestreo = FAST;
			}
			else if(velocidad_muestreo == FAST)
			{
				velocidad_muestreo = SLOW;
			}
		}
	}

	if(Chip_GPIO_GetPinState(LPC_GPIO, BOTON_MAX_MIN)==BOTON_ON)
	{
		vTaskDelay(DELAY_REBOTE_MS / portTICK_RATE_MS);
		if(Chip_GPIO_GetPinState(LPC_GPIO, BOTON_MAX_MIN)==BOTON_ON)
		{
			if (operacion == NORMAL)
			{
				operacion = MAX;
			}
			else if(operacion == MAX)
			{
				operacion = MIN;
			}
			else if(operacion == MIN)
			{
				operacion = NORMAL;
			}
		}
	}

	if(Chip_GPIO_GetPinState(LPC_GPIO, BOTON_HOLD)==BOTON_ON)
	{
		vTaskDelay(DELAY_REBOTE_MS / portTICK_RATE_MS);
		if(Chip_GPIO_GetPinState(LPC_GPIO, BOTON_HOLD)==BOTON_ON)
		{
			if (estado_menu == HOLD)
			{
				estado_menu = ON;
				Clear_LCD();
			}
			else if (estado_menu != OFF)
			{
				estado_menu = HOLD;
				Print_LCD(" <H>");
			}

		}
	}

}




static void Teclado(void * a)
{

	configteclado();
	configlcd();
	adcInit();

	estado_menu = OFF;
	velocidad_muestreo = FAST;
	operacion = NORMAL;

	char str[15];
	uint32_t promedio = 0;
	uint32_t adc_samples;

	xSemaphoreTake(sem_adc, (portTickType) 0);

	while (1)
	{

		leerteclado();

		switch(estado_menu)
		{
			case OFF:	Board_LED_Set(0, true);
						Clear_LCD();
						break;

			case ON:
						Board_LED_Set(0, false);

						if(velocidad_muestreo == FAST)
							adc_samples = 2000;
						else
							adc_samples = 10000;

						for(uint32_t i = 0; i < adc_samples ; i++ )
						{
							xSemaphoreTake(sem_adc, portMAX_DELAY);

							promedio += valor_adc;
						}

						promedio /= adc_samples;


						Return_Home_LCD();

						uint32_t valor;

						if (operacion == NORMAL)
						{
							valor = promedio;
						}
						if (operacion == MAX)
						{
							if (promedio_ant > promedio)
								valor = promedio_ant;
							else
								valor = promedio;
						}
						if (operacion == MIN)
						{
							if (promedio_ant < promedio)
								valor = promedio_ant;
							else
								valor = promedio;
						}

						sprintf(str, "dB: %d    ", Convertir_DB(valor));
						Print_LCD(str);


						if (operacion == NORMAL)
							Print_LCD("N");
						if (operacion == MAX)
							Print_LCD("M");
						if (operacion == MIN)
							Print_LCD("m");

						if(velocidad_muestreo == FAST)
							Print_LCD("F ");
						if(velocidad_muestreo == SLOW)
							Print_LCD("S ");

						promedio_ant = valor;

						sprintf(str, "\r\ndB: %d \r\n", Convertir_DB(valor));

						vcom_write((uint8_t *)str, 10);

						break;


			case HOLD:
						break;

			default: estado_menu = OFF;
					 break;
		}

	}

}







/*==================[external functions definition]==========================*/

int main(void)
{
	initHardware();

	vSemaphoreCreateBinary(sem_adc);

	xTaskCreate(Teclado, (const char *)"teclado", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(cdcTask, (signed const char *)"cdc", 1024, 0, tskIDLE_PRIORITY+1, 0);

	vTaskStartScheduler();

	while (1) {
	}


}


/** @} doxygen end group definition */

/*==================[end of file]============================================*/


